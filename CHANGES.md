# CHANGES

## 0.5.2 - 2018-04-07
* Converted API from callbacks to async/await (promises)

## 0.5.1 - 2018-04-07
* First public release (limited API)

