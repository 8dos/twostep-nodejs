"use strict";

const request = require("request-promise");
const jwt = require("jsonwebtoken");

const VERSION = require("../package.json").version;
const LIVE_API_URL = "https://api.twostep.io";
const SANDBOX_API_URL = "http://localhost:4000"; // "https://api.twostep.io";
const USER_AGENT = "twostep-nodejs/" + VERSION;
const DEFAULT_TIMEOUT = 30; // seconds

class AbstractClient {

  /**
   * Creates a new REST client.
   * 
   * @constructor
   * @param {object} options 
   */
  constructor(options) {
    const opts = options || {};
    this.clientId = opts.clientId;
    this.clientSecret = opts.clientSecret;
    this.apiUrl = opts.apiUrl || LIVE_API_URL;
    if (opts.sandbox) {
      this.apiUrl = SANDBOX_API_URL;
    }
    this.timeout = opts.timeout || DEFAULT_TIMEOUT;
    this.tokenStore = opts.tokenStore;
  }

  /**
   * Executes a HTTP GET.
   * 
   * @param {string} path 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _get(path) {
    return await this._requestWithToken("GET", path, null);
  }

  /**
   * Executes a HTTP POST.
   * 
   * @param {string} path 
   * @param {object} params 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _post(path, params) {
    return await this._requestWithToken("POST", path, params);
  }

  /**
   * Executes a HTTP PUT.
   * 
   * @param {string} path 
   * @param {object} params 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   *
  async _put(path, params) {
    return await this._requestWithToken("PUT", path, params);
  }
*/

  /**
   * Executes a HTTP DELETE.
   * 
   * @param {string} path 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _delete(path) {
    return await this._requestWithToken("DELETE", path, null);
  }

  //--------------------------------------------------------------------

  /**
   * Obtains an access token and sends a HTTP request.
   * 
   * @param {string} method 
   * @param {string} path 
   * @param {object} params 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _requestWithToken(method, path, params) {
    let token = await this._getToken();
    return await this._request(method, path, params, token);
  }

  /**
   * Sends a HTTP request.
   * 
   * @param {string} method 
   * @param {string} path 
   * @param {object} params 
   * @param {string} token 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _request(method, path, params, token) {
    let headers = {
      "User-Agent": USER_AGENT,
      "Accept": "application/json"
    };

    if (params != null) {
      headers["Content-Type"] = "application/json";
    }

    if (token != null) {
      headers["Authorization"] = "Bearer " + token.access_token;
    }

    let options = {
      method: method,
      url: this.apiUrl + path,
      headers: headers,
      json: params ? params : true,
      jar: false,
      timeout: this.timeout * 1000,
      resolveWithFullResponse: true //NOTE: Return full response instead of just the body
    };

    try {
      let res = await request(options);
      return res.body;
    } catch (ex) {
      let err = ex.error;
      /*if (ex.error.code === "ESOCKETTIMEDOUT") {
        console.log("----------> TIMED OUT <------------");
      } else if (ex.error.code === "ENOTFOUND") {
        console.log("----------> SERVER NOT FOUND <------------");
      } else if (ex.error.code === "ECONNREFUSED") {
        console.log("----------> CONNECTION REFUSED <------------");
      }*/
      throw err;
    }
  }

  /**
   * Gets an access token.
   *
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _getToken() {
    if (!this.clientId) {
      throw new Error("No Client ID provided");
    }
    if (!this.clientSecret) {
      throw new Error("No Client secret provided");
    }

    let token = null;
    if (this.tokenStore) {
      token = this.tokenStore.load();
    }

    if (this._isTokenValid(token)) {
      if (this._isTokenExpired(token)) {
        token = await this._refreshToken(token)
      }
      return token;
    }
    return await this._newToken();
  }

  /**
   * Gets a new access token from the server.
   * 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _newToken() {
    let params = {
      client_id: this.clientId,
      client_secret: this.clientSecret,
      grant_type: "client_credentials"
    };

    let body = await this._request("POST", "/auth/token", params, null);
    if (this.tokenStore) {
      this.tokenStore.save(body);
    }
    return body;
  }

  /**
   * Gets a new access token using the refresh token.
   * 
   * @param {object} oldToken 
   * @returns {object} JSON object.
   * @throws {object} JSON object with error message.
   */
  async _refreshToken(oldToken) {
    let params = {
      client_id: this.clientId,
      client_secret: this.clientSecret,
      grant_type: "refresh_token",
      refresh_token: oldToken.access_token
    };

    let body = await this._request("POST", "/auth/token", params, null);
    if (this.tokenStore) {
      this.tokenStore.save(body);
    }
    return body;
  }

  /**
   * Check if token has access_token and refresh_token attributes set.
   * 
   * @param {object} token 
   * @returns {bool} - true if token is valid, false otherwise
   */
  _isTokenValid(token) {
    return (token &&
      "access_token" in token && "refresh_token" in token &&
      token.access_token != null && token.refresh_token != null &&
      token.access_token.length > 0 && token.refresh_token.length > 0);
  }

  /**
   * Decode and check if the token has expired.
   * 
   * @param {object} token 
   * @returns {bool} - true if token has expired, false otherwise
   */
  _isTokenExpired(token) {
    let decoded = jwt.decode(token.access_token);
    if (decoded) {
      let exp = parseInt(decoded["exp"], 10) || 0;
      let now = (new Date).getTime() / 1000;
      if (exp > now) {
        return false; // still valid
      }
    }
    return true; // expired or invalid
  }
}

module.exports = AbstractClient;