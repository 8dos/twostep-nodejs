"use strict";

const querystring = require("querystring");
const AbstractClient = require("./base");

/**
 * Twostep API client.
 */
class Twostep extends AbstractClient {

  /**
   * Creates a new user.
   * 
   * @param {string} email 
   * @param {string} phone 
   * @param {number} countryCode 
   * @param {bool} sendInstallLink 
   * @returns {User} 
   */
  async createUser(email, phone, countryCode, sendInstallLink) {
    let payload = {
      email: email,
      phone: phone,
      country_code: countryCode,
      send_install_link: sendInstallLink
    };
    return await this._post("/v1/users", payload);
  }

  /**
   * Get the status for a given user.
   * 
   * @param {string} userId 
   * @returns {object} user details
   */
  async getUser(userId) {
    return await this._get("/v1/users/" + querystring.escape(userId));
  }

  /**
   * Requests a SMS to be sent to the given user.
   * 
   * @param {string} userId 
   * @param {bool} force 
   * @returns {object} receipt
   */
  async requestSms(userId, force) {
    let payload = {
      force: force
    }
    return await this._post("/v1/users/" + querystring.escape(userId) + "/sms", payload);
  }

  /**
   * Requests a voice call to the given user.
   * 
   * @param {string} userId 
   * @param {bool} force 
   * @returns {object} receipt
   */
  async requestCall(userId, force) {
    let payload = {
      force: force
    }
    return await this._post("/v1/users/" + querystring.escape(userId) + "/call", payload);
  }

  /**
   * Removes the given user.
   * 
   * @param {string} userId 
   * @returns {object} user details
   */
  async removeUser(userId) {
    return await this._delete("/v1/users/" + querystring.escape(userId));
  }
}

module.exports = Twostep;