"use strict";

const chai = require("chai");
const assert = chai.assert;
const expect = chai.expect;
const should = chai.should();

const Twostep = require("../lib/twostep");

const CLIENT_ID = "12345";
const CLIENT_SECRET = "67890";

class TokenStore {
  constructor() {
    this.token = null;
  }

  load() {
    // console.log("Loading token from store");
    // if (this.token && this.token.access_token) {
    //   console.log("Found token....");
    // }
    return this.token;
  }

  save(token) {
    // console.log("Saving token to store:");
    // console.log(token);
    this.token = token;
  }

  clear() {
    this.token = null;
  }
}

class ExpiredTokenStore {
  constructor() {}

  load() {
    // Return an expired token
    return {
      "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A",
      "token_type": "Bearer",
      "scope": "none",
      "expires_in": 3600,
      "refresh_token": "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f"
    };
  }

  save(token) {}

  clear() {}
}

class BadTokenStore {
  constructor() {}

  load() {
    // Return a "broken" token
    return {
      "access_token": "this.willnot.work",
      "token_type": "Bearer",
      "scope": "none",
      "expires_in": 3600,
      "refresh_token": "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f"
    };
  }

  save(token) {}

  clear() {}
}

const client = new Twostep({
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  sandbox: true,
  tokenStore: new TokenStore()
});

describe("Client", function () {

  it("should get user", async function () {
    try {
      let user = await client.getUser("123456");
      should.exist(user);
      user.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should fail to get unknown user", async function () {
    try {
      let user = await client.getUser("000000");
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
    }
  });

  it("should get user using an expired token from store", async function () {
    let cli = new Twostep({
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      sandbox: true,
      tokenStore: new ExpiredTokenStore()
    });
    try {
      let user = await cli.getUser("123456");
      should.exist(user);
      user.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should get user using a bad token from store", async function () {
    let cli = new Twostep({
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      sandbox: true,
      tokenStore: new BadTokenStore()
    });
    try {
      let user = await cli.getUser("123456");
      should.exist(user);
      user.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should fail when server is down", async function () {
    let cli = new Twostep({
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      apiUrl: "http://localhost:4444"
    });
    try {
      let user = await cli.getUser("123456");
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
      //console.log(err);
    }
  });

  it("should fail to use a fake server", async function () {
    let cli = new Twostep({
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      apiUrl: "http://fakeserver:4000"
    });
    try {
      let user = await cli.getUser("123456");
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
      //console.log(err);
    }
  });

  it("should create a user", async function () {
    try {
      let user = await client.createUser("jdoe@example.com", "+12125551234", 1, false);
      should.exist(user);
      user.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should fail to create a user with invalid email", async function () {
    try {
      let user = await client.createUser("jdoeexample.com", "+12125551234", 1, false);
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
      err.should.be.an("object");
    }
  });

  it("should remove user", async function () {
    try {
      let user = await client.removeUser("123456");
      should.exist(user);
      user.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should fail to remove unknown user", async function () {
    try {
      let user = await client.removeUser("000000");
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
      err.should.be.an("object");
    }
  });

  it("should request sms", async function () {
    try {
      let receipt = await client.requestSms("123456", false);
      should.exist(receipt);
      receipt.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should fail to request sms for unknown user", async function () {
    try {
      let receipt = await client.requestSms("000000", false);
      should.not.exist(receipt);
    } catch (err) {
      should.exist(err);
      err.should.be.an("object");
    }
  });

  it("should request call", async function () {
    try {
      let receipt = await client.requestCall("123456", false);
      should.exist(receipt);
      receipt.should.be.an("object");
    } catch (err) {
      should.not.exist(err);
    }
  });

  it("should fail to request call for unknown user", async function () {
    try {
      let receipt = await client.requestCall("000000", false);
      should.not.exist(receipt);
    } catch (err) {
      should.exist(err);
      err.should.be.an("object");
    }
  });

  //*************************************************************************
  // Should be the last tests!

  it("should fail to use api without valid client id", async function () {
    let cli = new Twostep({
      clientId: "",
      sandbox: true
    });
    try {
      let user = await cli.getUser("123456");
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
    }
  });

  it("should fail to use api without valid client secret", async function () {
    let cli = new Twostep({
      clientId: CLIENT_ID,
      sandbox: true
    });
    try {
      let user = await cli.getUser("123456");
      should.not.exist(user);
    } catch (err) {
      should.exist(err);
    }
  });

});